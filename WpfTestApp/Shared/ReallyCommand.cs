﻿using System;
using System.Windows.Input;

namespace WpfTestApp.Shared
{
    public class ReallyCommand : ICommand
    {
        // Fields to store the action to execute and the function to check if the command can be executed
        private readonly Action<object> _action;
        private readonly Func<object, bool> _func;

        // Event to notify when the execution status of the command has changed
        public event EventHandler CanExecuteChanged;

        // Constructor for commands that do not require a parameter
        public ReallyCommand(Action action)
        {
            // Convert the action to an Action<object> delegate for convenience
            _action = (Action<object>)(_param1 => action());
        }

        // Constructor for commands that require a parameter
        public ReallyCommand(Action<object> action)
        {
            _action = action;
        }

        // Constructor for commands that require a parameter and a function to check if the command can be executed
        public ReallyCommand(Action<object> action, Func<object, bool> func)
        {
            _action = action;
            _func = func;
        }

        // Method to check if the command can be executed with the given parameter
        public bool CanExecute(object parameter)
        {
            return _func == null || _func(parameter);
        }

        // Method to execute the command with the given parameter
        public void Execute(object parameter)
        {
            _action(parameter);
        }

        // Method to execute the command without a parameter
        public void Execute()
        {
            _action((object)null);
        }
    }

}
