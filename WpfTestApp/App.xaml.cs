﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Windows;
using WpfTestApp.Infrastructure;
using WpfTestApp.Services;
using WpfTestApp.ViewModel;

namespace WpfTestApp
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        // Declare ServiceProvider property to be used throughout the application
        internal static ServiceProvider ServiceProvider { get; private set; }

        public App()
        {
            // Create new instance of ServiceCollection and pass it to the ConfigureServices method
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            // Build the ServiceProvider using the ServiceCollection
            ServiceProvider = serviceCollection.BuildServiceProvider();
        }

        private void ConfigureServices(IServiceCollection services)
        {
            // Add AuthService as a singleton service
            services.AddSingleton<AuthService>();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            // Show the login view model using the ViewManager
            ViewManager.Show(new LoginViewModel());

            // Add an event handler for unhandled exceptions
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            base.OnStartup(e);
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            // Write the exception details to a file when an unhandled exception occurs
            var ex = (Exception)e.ExceptionObject;
            using var sw = new StreamWriter(Environment.CurrentDirectory + @"\Crash.txt", false, System.Text.Encoding.UTF8);
            sw.WriteLine(ex.Message);
            sw.WriteLine("================================");
            sw.WriteLine(ex.StackTrace);
            sw.WriteLine("================================");
            sw.WriteLine(ex.InnerException?.Message);
            sw.WriteLine("================================");
            sw.WriteLine(ex.InnerException?.StackTrace);
        }
    }
}
