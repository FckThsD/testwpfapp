﻿using System.ComponentModel;

namespace WpfTestApp.Infrastructure
{
    // This is a base class for a viewmodel that implements the INotifyPropertyChanged interface
    public class ModelBase : INotifyPropertyChanged
    {
        // This event is raised when a property value changes
        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called to raise the PropertyChanged event with the given property name
        public virtual void OnPropertyChanged(string propertyName)
        {
            // Retrieve the current list of event subscribers
            var propertyChanged = PropertyChanged;

            // If there are any subscribers, invoke the event with the sender object and the PropertyChangedEventArgs object
            propertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
