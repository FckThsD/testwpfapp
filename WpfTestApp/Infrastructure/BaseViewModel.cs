﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using MahApps.Metro.Controls.Dialogs;
using WpfTestApp.Shared;

namespace WpfTestApp.Infrastructure
{
    // Base class for ViewModels that provides some common functionality
    public abstract class BaseViewModel : ModelBase
    {
        #region Protected Interfaces + Fields

        // A reference to the application dispatcher
        protected readonly Dispatcher AppDispatcher;

        // A command that is executed when the window is loaded
        public ICommand WindowLoaded { get; set; }

        #endregion Protected Interfaces + Fields

        #region Private Interfaces + Fields

        // A reference to the dialog manager for showing dialogs
        private readonly IDialogCoordinator _dialogManager;

        #endregion Private Interfaces + Fields

        #region Public Constructors


        // Constructor for BaseViewModel
        protected BaseViewModel()
        {
            // Get a reference to the dialog manager
            _dialogManager = DialogCoordinator.Instance;

            // Get a reference to the application dispatcher
            AppDispatcher = Application.Current.Dispatcher;

            // Create a command that will execute the Loaded method asynchronously
            WindowLoaded = new ReallyCommand(async () => { await Loaded(); });
        }

        #endregion Public Constructors

        #region Public Properties

        // A boolean that indicates if there is an animation playing
        public bool IsAnimation { get; set; }

        // A boolean that indicates if the application is currently loading
        public bool IsLoading { get; set; }

        #endregion Public Properties

        #region Public Methods

        // Virtual method to be overridden in child classes for closing the view
        public virtual void Close()
        {

        }

        // Virtual method to be overridden in child classes for executing code when the view is loaded
        public virtual async Task Loaded()
        {

        }

        #endregion Public Methods

        #region Protected Methods

        // Shows the next view and closes the current view
        protected void NextStep(BaseViewModel viewModel)
        {
            ViewManager.Show(viewModel);
            ViewManager.Close(this);
        }

        // Shows an error dialog asynchronously
        protected async Task ShowErrorDialogAsync(string errorMessage)
        {
            var viewModel = this;
            await viewModel.ShowErrorDialogAsync(viewModel, errorMessage);
        }

        // Shows an error dialog asynchronously with an exception message
        protected async Task ShowErrorDialogAsync(string errorMessage, Exception exception)
        {
            await ShowErrorDialogAsync(this, errorMessage, exception);
        }

        // Shows an error dialog asynchronously for a specific view model
        protected async Task ShowErrorDialogAsync(BaseViewModel viewModel, string errorMessage)
        {
            var isAnimation = IsAnimation;
            var num = (int)await _dialogManager.ShowMessageAsync((object)viewModel, "Error", errorMessage, MessageDialogStyle.Affirmative, new MetroDialogSettings()
            {
                AnimateHide = isAnimation,
                AnimateShow = isAnimation
            });
        }
        // Shows an error dialog asynchronously for a specific view model with an exception message
        protected async Task ShowErrorDialogAsync(
          BaseViewModel viewModel,
          string errorMessage,
          Exception exception)
        {
            var isAnimation = IsAnimation;
            var num = (int)await _dialogManager.ShowMessageAsync((object)viewModel, "Error", errorMessage + Environment.NewLine + exception.Message, MessageDialogStyle.Affirmative, new MetroDialogSettings()
            {
                AnimateHide = isAnimation,
                AnimateShow = isAnimation
            });
        }

        // Shows an input dialog asynchronously with a default text
        protected async Task<string> ShowInputAsync(
          string title,
          string message,
          string defaultText)
        {
            return await ShowInputAsync(title, message, new MetroDialogSettings()
            {
                DefaultText = defaultText
            });
        }

        // Shows an input dialog asynchronously with custom settings
        protected async Task<string> ShowInputAsync(
          string title,
          string message,
          MetroDialogSettings settings)
        {
            settings.AnimateShow = settings.AnimateHide = IsAnimation;
            return await _dialogManager.ShowInputAsync(this, title, message, settings);
        }

        // Shows an input dialog asynchronously with default settings
        protected async Task<string> ShowInputAsync(string title, string message)
        {
            return await ShowInputAsync(title, message, new MetroDialogSettings());
        }

        // Shows a login dialog asynchronously with custom settings
        protected async Task<LoginDialogData> ShowLoginAsync(
          string title,
          string message,
          LoginDialogSettings settigs)
        {
            settigs.AnimateShow = settigs.AnimateHide = IsAnimation;
            return await _dialogManager.ShowLoginAsync(this, title, message, settigs);
        }

        // Shows a message dialog asynchronously with custom settings
        protected async Task ShowMessageDialogAsync(
          string title,
          string message,
          string buttonText)
        {
            var isAnimation = IsAnimation;
            var num = (int)await _dialogManager.ShowMessageAsync(this, title, message, MessageDialogStyle.Affirmative, new MetroDialogSettings()
            {
                AnimateHide = isAnimation,
                AnimateShow = isAnimation,
                AffirmativeButtonText = buttonText
            });
        }

        // Shows a success message dialog asynchronously
        protected async Task ShowSuccessDialogAsync(string successMessage)
        {
            var isAnimation = IsAnimation;
            var num = (int)await _dialogManager.ShowMessageAsync(this, "Success", successMessage, MessageDialogStyle.Affirmative, new MetroDialogSettings()
            {
                AnimateHide = isAnimation,
                AnimateShow = isAnimation
            });
        }

        // Shows a message dialog asynchronously with affirmative and negative buttons
        protected Task<MessageDialogResult> ShowYesOrNoDialogAsync(string title, string message)
        {
            var isAnimation = IsAnimation;
            return _dialogManager.ShowMessageAsync(this, title, message, MessageDialogStyle.AffirmativeAndNegative, new MetroDialogSettings()
            {
                AnimateHide = isAnimation,
                AnimateShow = isAnimation
            });
        }

        // Waits for a specified period of time
        public static async Task Sleep(TimeSpan loadLimit, TimeSpan elapsedTime)
        {
            var delay = loadLimit - elapsedTime;
            if (delay.TotalMilliseconds <= 0.0)
                return;
            await Task.Delay(delay);
        }

        #endregion Protected Methods

    }
}
