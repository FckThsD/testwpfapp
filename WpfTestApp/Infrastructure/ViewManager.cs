﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Windows;
using WpfTestApp.View.Interfaces;

namespace WpfTestApp.Infrastructure
{
    // A utility class for managing views and view models
    public static class ViewManager
    {
        // A dictionary to store the relationship between view models and their corresponding windows
        private static readonly Dictionary<BaseViewModel, Window> Windows = new Dictionary<BaseViewModel, Window>();

        // Shows the window corresponding to the given view model
        public static Window Show([NotNull] BaseViewModel viewModel)
        {
            return Show(viewModel, null);
        }

        // Shows the window corresponding to the given view model with additional arguments
        public static Window Show([NotNull] BaseViewModel viewModel, params object[] args)
        {
            var window = CreateWindow(viewModel, args);
            window.Show();
            return window;
        }

        // Shows the window corresponding to the given view model as a modal dialog
        public static void ShowDialog([NotNull] BaseViewModel viewModel)
        {
            var window = CreateWindow(viewModel, null);
            window.ShowDialog();
        }

        // Creates a window based on the given view model
        private static Window CreateWindow([NotNull] BaseViewModel viewModel, params object[] args)
        {
            // Get the full name of the view model and remove the "Model" suffix to get the name of the view
            var fullName = viewModel.GetType().FullName;
            if (fullName == null)
                throw new ArgumentNullException($"ViewManager , viewModel==null");
            var type = Type.GetType(fullName.Replace("Model", "") ?? "", true);
            if (type == null)
                throw new ArgumentNullException($"ViewManager , type==null");

            // Create an instance of the view and set its data context to the view model
            if (!(Activator.CreateInstance(type, args) is Window instance))
                throw new ArgumentNullException($"ViewManager , view==null");
            instance.DataContext = viewModel;

            // Handle the view's Closed event to close the view model when the view is closed
            instance.Closed += (EventHandler)((param1, param2) => viewModel.Close());

            // Check if the view implements the IBaseView interface, and set its view model property
            if (!(instance is IBaseView baseView))
                throw new ArgumentException("ViewManager , ViewModel is not IBaseView");
            baseView.ViewModel = viewModel;

            // Add the view model and window to the dictionary
            try
            {
                Windows.Add(viewModel, instance);
            }
            catch (Exception ex)
            {

            }


            return instance;
        }

        // Closes the window corresponding to the given view model
        public static bool Close(BaseViewModel viewModel)
        {
            if (!Windows.ContainsKey(viewModel))
                return false;

            // Remove the view model and window from the dictionary and close the window
            var window = Windows[viewModel];
            Windows.Remove(viewModel);
            window.Dispatcher.Invoke(() =>
            {
                window.Close();

            });
            return true;
        }
    }
}
