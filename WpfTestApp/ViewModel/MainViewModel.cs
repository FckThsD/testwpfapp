﻿using WpfTestApp.Infrastructure;

namespace WpfTestApp.ViewModel
{
    public class MainViewModel : BaseViewModel
    {
        // Add private variables
        // private readonly SomeService _someService;

        public MainViewModel()
        {
            // Initialize services
            //_someService = App.ServiceProvider.GetService<SomeService>();

            // Assign values
            // SomeCommand = new ReallyCommand(SomeAsync);
        }

        // Add models and commands
        // public SomeModel SomeModel { get; set; } = new SomeModel();
        // public ReallyCommand SomeCommand { get; set; }

        // Create functions for commands
        // private async void SomeAsync()
        // {
        // }
    }
}
