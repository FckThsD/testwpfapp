﻿using Microsoft.Extensions.DependencyInjection;
using System;
using WpfTestApp.Infrastructure;
using WpfTestApp.Model;
using WpfTestApp.Services;
using WpfTestApp.Shared;

namespace WpfTestApp.ViewModel
{
    public class LoginViewModel : BaseViewModel
    {
        // Declare a private field of type AuthService to hold a reference to an instance of that class
        private readonly AuthService _authService;

        public LoginViewModel()
        {
            // Get an instance of AuthService from the application's service provider
            _authService = App.ServiceProvider.GetService<AuthService>();

            // Initialize a new instance of ReallyCommand, passing in the LoginAsync method as the command's action
            LoginCommand = new ReallyCommand(LoginAsync);
        }

        // Declare a public property of type LoginModel to hold the current user account
        public LoginModel CurrentAccount { get; set; } = new LoginModel();

        // Declare a public property of type ReallyCommand to hold the command that will execute the login action
        public ReallyCommand LoginCommand { get; set; }

        // Define an async method to execute the login action
        private async void LoginAsync()
        {
            if (!CurrentAccount.IsFull)// Check if all required fields are filled in the CurrentAccount instance
            {
                await ShowErrorDialogAsync("All fields must be filled in!");// If not, show an error message and return
                return;
            }

            try
            {
                // Call the Login method on the AuthService instance, passing in the CurrentAccount instance
                await _authService.Login(CurrentAccount);

                // Show the main view model and close the login view model
                NextStep(new MainViewModel());

            }
            catch (Exception ex)// If an exception is thrown, catch it
            {
                await ShowErrorDialogAsync(this, ex.Message);// Show an error message with the exception message
            }
        }
    }
}
