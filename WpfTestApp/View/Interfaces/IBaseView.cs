﻿using WpfTestApp.Infrastructure;

namespace WpfTestApp.View.Interfaces
{
    public interface IBaseView
    {
        BaseViewModel ViewModel { get; set; }

        void Close();
    }
}
