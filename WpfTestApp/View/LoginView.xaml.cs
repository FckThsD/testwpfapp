﻿using MahApps.Metro.Controls;
using System.Windows.Markup;
using WpfTestApp.Infrastructure;
using WpfTestApp.View.Interfaces;

namespace WpfTestApp.View
{
    /// <summary>
    /// Логика взаимодействия для LoginView.xaml
    /// </summary>
    public partial class LoginView : MetroWindow, IBaseView, IComponentConnector
    {
        // Property to hold the ViewModel for this view
        public BaseViewModel ViewModel { get; set; }

        // Constructor for the LoginView
        public LoginView()
        {
            InitializeComponent();
        }

        // Close method to be implemented from the IBaseView interface
        void IBaseView.Close()
        {
            Close();
        }
    }
}
