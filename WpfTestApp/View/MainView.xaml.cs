﻿using MahApps.Metro.Controls;
using System.Windows.Markup;
using WpfTestApp.Infrastructure;
using WpfTestApp.View.Interfaces;

namespace WpfTestApp.View
{
    /// <summary>
    /// Логика взаимодействия для MainView.xaml
    /// </summary>
    public partial class MainView : MetroWindow, IBaseView, IComponentConnector
    {
        // Property to hold the ViewModel for this view
        public BaseViewModel ViewModel { get; set; }

        // Constructor for the MainView
        public MainView()
        {
            InitializeComponent();
        }

        // Close method to be implemented from the IBaseView interface
        void IBaseView.Close()
        {
            Close();
        }
    }
}
