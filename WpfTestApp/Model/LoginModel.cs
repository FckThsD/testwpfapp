﻿using WpfTestApp.Infrastructure;

namespace WpfTestApp.Model
{
    public class LoginModel : ModelBase
    {
        public bool IsFull => !string.IsNullOrWhiteSpace(Login) && !string.IsNullOrWhiteSpace(Password);
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
